package jp.alhinc.matano_satsuki.caluculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SalesSummary {

    /* 支店定義ファイル名定義 */
    private final static String BRA_LST_NAME = "branch.lst";
    private final static String BRA_FILE_NAME = "支店定義ファイル";
    /* 支店コードフォーマット */
    private final static String STORE_CD_FORMAT = "^[0-9]{3}$";
    /* 商品定義ファイル名定義 */
    private final static String COM_LST_NAME = "commodity.lst";
    private final static String COM_FILE_NAME = "商品定義ファイル";
    /* 商品コードフォーマット */
    private final static String COM_CD_FORMAT = "^[A-Za-z0-9]{8}$";
    /* 支店集計結果ファイル名定義 */
    private final static String BRA_OUT_FILE_NAME = "branch.out";
    /* 商品集計結果ファイル名定義 */
    private final static String COM_OUT_FILE_NAME = "commodity.out";
    /* 売上ファイル拡張子 */
    private final static String EXTENTION = ".rcd";
    /* 売上ファイル名桁数 */
    private final static Integer SALESFILE_LEN = 8;
    /* 売上ファイル名フォーマット */
    private final static String SALESFILE_FORMAT = "^[0-9]{"+ SALESFILE_LEN +"}" + EXTENTION + "$";
    /* 合計金額上限桁数 */
    private final static Integer MAX_LENGTH = 10;
    /* 文字コード */
    private final static String UTF8 = "UTF-8";

    /* エラーメッセージ定義 */
    private final static String COMMON_ERR = "予期せぬエラーが発生しました";
    private final static String FILE_ERR = "が存在しません";
    private final static String FILE_FORMAT_ERR = "のフォーマットが不正です";
    private final static String FILE_NAME_ERR = "売上ファイル名が連番になっていません";
    private final static String STORE_CD_ERR = "の支店コードが不正です";
    private final static String COM_CD_ERR = "の商品コードが不正です";
    private final static String MAX_LENGTH_ERR = "合計金額が10桁を超えました";

    /**
     * メイン処理実行開始
     * */
    public static void main(String[] args){
        /* 支店定義 */
        Map<String , String> store_map = new HashMap<>();
        /* 商品定義 */
        Map<String , String> commodity_map = new HashMap<>();
        /* 支店毎売上情報リスト */
        Map<String, List<Long>> store_salesListMap = new HashMap<>();
        /* 商品毎売上情報リスト */
        Map<String, List<Long>> commodity_salesListMap = new HashMap<>();
        /* 支店毎売上合計マップ */
        Map<String, Long> store_totalMap = new HashMap<>();
        /* 商品毎売上合計マップ */
        Map<String, Long> commodity_totalMap = new HashMap<>();

        // 定義ファイル読み込み
        if(!getStoreMap(new File(args[0], BRA_LST_NAME), store_map, BRA_FILE_NAME, STORE_CD_FORMAT)){
            return;
        }
        if(!getStoreMap(new File(args[0], COM_LST_NAME), commodity_map, COM_FILE_NAME, COM_CD_FORMAT)){
            return;
        }

        // 連番チェック
        List<String> salesFileList = new ArrayList<>();
        File[] targetFiles = new File(args[0]).listFiles();
        for (int i = 0; i < targetFiles.length; i++) {
            String salesFileNm = targetFiles[i].getName();
            if (!salesFileNm.matches(SALESFILE_FORMAT)){
                continue;
            }
            salesFileList.add(salesFileNm.substring(0, SALESFILE_LEN));
        }
        if (salesFileList.size() <= 0) {
            System.out.println(COMMON_ERR);
            return;
        }
        Collections.sort(salesFileList);
        for (int cnt = 1; cnt < salesFileList.size(); cnt++) {
            if(Integer.parseInt(salesFileList.get(0)) + cnt != Integer.parseInt(salesFileList.get(cnt))){
                System.out.println(FILE_NAME_ERR);
                return;
            }
        }

        // 売上ファイル読み込み
        String filename = "";
        String[] buffList;
        buffList = new String[3];
        for (int fileCnt = 0; fileCnt < salesFileList.size(); fileCnt++) {
            filename = salesFileList.get(fileCnt) + EXTENTION;
            List<Long> store_salesList = new ArrayList<>();
            List<Long> commodity_salesList = new ArrayList<>();
            BufferedReader salesBuff = null;
            String line = "";
            int n = 0;
            try {
                salesBuff = new BufferedReader(new InputStreamReader(
                            new FileInputStream(args[0] + "\\" + salesFileList.get(fileCnt) + EXTENTION), UTF8));
                while((line = salesBuff.readLine()) != null){
                    if(n > 2){
                        break;
                    }
                    buffList[n] = line;
                    n++;
                }
                if(n != 3){
                    System.out.println(filename + FILE_FORMAT_ERR);
                    return;
                }else if(!store_map.containsKey(buffList[0])){
                    System.out.println(filename + STORE_CD_ERR);
                    return;
                }else if(!commodity_map.containsKey(buffList[1])){
                    System.out.println(filename + COM_CD_ERR);
                    return;
                }
                if(!store_salesListMap.containsKey(buffList[0])){
                    store_salesListMap.put(buffList[0], store_salesList);
                }
                if(!commodity_salesListMap.containsKey(buffList[1])){
                    commodity_salesListMap.put(buffList[1], commodity_salesList);
                }
                store_salesList = store_salesListMap.get(buffList[0]);
                store_salesList.add(Long.parseLong(buffList[2]));
                commodity_salesList = commodity_salesListMap.get(buffList[1]);
                commodity_salesList.add(Long.parseLong(buffList[2]));
            } catch (NumberFormatException e) {
                System.out.println(filename + FILE_FORMAT_ERR);
                return;
            } catch (Exception e) {
                System.out.println(COMMON_ERR);
                return;
            } finally {
                try {
                    salesBuff.close();
                } catch (IOException e) {
                    System.out.println(COMMON_ERR);
                    return;
                }
            }
        }

        // 集計
        if(!calc(store_salesListMap,store_totalMap)){
            return;
        }
        if(!calc(commodity_salesListMap,commodity_totalMap)){
            return;
        }

        // 結果ファイル出力
        if(!outPut(args[0] +"\\"+ BRA_OUT_FILE_NAME, store_totalMap, store_map)){
            return;
        }
        if(!outPut(args[0] +"\\"+ COM_OUT_FILE_NAME, commodity_totalMap, commodity_map)){
            return;
        }
    }

    /**
     * 定義ファイル取得
     * @param targetFile,lst_map,lst_name,format
     * @return 正常：true / 異常：false
     */
    public static boolean getStoreMap(File targetFile, Map<String , String> lst_map, String lst_name, String format){
        if (!targetFile.exists()){
            System.out.println(lst_name + FILE_ERR);
            return false;
        }
        BufferedReader buff = null;
        try {
            buff = new BufferedReader(new InputStreamReader(new FileInputStream(targetFile), UTF8));
            String line = "";
            while ((line = buff.readLine()) != null) {
                String[] lineArr = line.split(",");
                if (lineArr.length != 2 || !lineArr[0].matches(format) ||
                    lst_map.containsKey(lineArr[0])) {
                    System.out.println(lst_name +FILE_FORMAT_ERR);
                    return false;
                }
                lst_map.put(lineArr[0],lineArr[1]);
            }
        } catch (FileNotFoundException err1) {
            System.out.println(lst_name + FILE_FORMAT_ERR);
            return false;
        } catch (Exception err2){
            System.out.println(COMMON_ERR);
            return false;
        } finally {
            try {
                buff.close();
            } catch (IOException err) {
                // 何もしない
            }
        }
        if (lst_map.size() <= 0) {
            System.out.println(COMMON_ERR);
            return false;
        }
        return true;
    }

    /**
     * 集計
     * @param salesListMap,totalMap
     * @return 正常：true / 異常：false
     */
    private static boolean calc(Map<String,List<Long>> salesListMap, Map<String,Long> totalMap){
        String code = "";
        Iterator<String> ite_storeCD = salesListMap.keySet().iterator();
        while(ite_storeCD.hasNext()){
            Long totalVal = 0L;
            List<Long> salesList = new ArrayList<>();
            code = ite_storeCD.next();
            salesList = salesListMap.get(code);
            for(int i = 0; i < salesList.size(); i++){
                totalVal = totalVal + salesList.get(i);
                if(totalVal.toString().length() > MAX_LENGTH){
                    System.out.println(MAX_LENGTH_ERR);
                    return false;
                }
            }
            totalMap.put(code, totalVal);
        }
        return true;
    }

    /**
     * 集計ファイル出力(金額降順)
     * @param filename,totalMap
     * @return 正常：true / 異常：false
     */
    private static boolean outPut(String filename, Map<String, Long> totalMap, Map<String, String> defMap){
        // List 生成 (ソート用)
        List<Entry<String, Long>> entries = new ArrayList<Map.Entry<String, Long>>(totalMap.entrySet());
        Collections.sort(entries, new Comparator<Map.Entry<String, Long>>() {
            @Override
            public int compare(Entry<String, Long> entry1, Entry<String,Long> entry2) {
                return ((Long)entry2.getValue()).compareTo((Long)entry1.getValue());
            }
        });
        String code = "";
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(filename), UTF8));
            for(Entry<String, Long> s: entries){
                code = s.getKey();
                writer.println(code +","+ defMap.get(code) + "," + totalMap.get(code));
            }
        } catch (IOException e) {
            System.out.println(COMMON_ERR);
            return false;
        } finally {
            try {
                writer.close();
            } catch(Exception err) {
                //何もしない
            }
        }
        return true;
    }
}
